
class SelectionSort:

    def __init__(self):
        pass

    @staticmethod
    def sort(array):
        for i in range(len(array) - 1, 0, -1):
            max_position = 0
            for j in range(1, i + 1):
                if array[j] > array[max_position]:
                    max_position = j
            array[i], array[max_position] = array[max_position], array[i]
        return array
