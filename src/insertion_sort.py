
class InsertionSort:

    def __init__(self):
        pass

    @staticmethod
    def sort(array):
        for index in range(1, len(array)):
            valor = array[index]
            max_size = index
            while max_size > 0 and array[max_size - 1] > valor:
                array[max_size] = array[max_size - 1]
                max_size -= 1
            array[max_size] = valor
        return array
