
import os
import csv

class CSVGenerator:

    def __init__(self):
        self.csv_directory = "./csv"

        if not os.path.isdir(self.csv_directory):
            os.makedirs(self.csv_directory)

        self.asc_csv_file = open('csv/ascending.csv', 'wb')
        self.desc_csv_file = open('csv/descending.csv', 'wb')
        self.random_csv_file = open('csv/random.csv', 'wb')

        self.ascending = csv.writer(self.asc_csv_file)
        self.descending = csv.writer(self.desc_csv_file)
        self.random = csv.writer(self.random_csv_file)

    def insert_header(self):
        header = "Tamanhos", "Bubble Sort", "Bubble Sort With Flag", \
                 "Insertion Sort", "Selection Sort", "Shell Sort", \
                 "Merge Sort", "Quick Sort", "Counting Sort", "Radix Sort"
        self.ascending.writerow(header)
        self.descending.writerow(header)
        self.random.writerow(header)

    def insert_time(self, size, asc_array_time, desc_array_time, random_array_time):

        asc_array_time.insert(0, size)
        desc_array_time.insert(0, size)
        random_array_time.insert(0, size)

        self.ascending.writerow(asc_array_time)
        self.descending.writerow(desc_array_time)
        self.random.writerow(random_array_time)

    def close(self):
        self.asc_csv_file.close()
        self.desc_csv_file.close()
        self.random_csv_file.close()
