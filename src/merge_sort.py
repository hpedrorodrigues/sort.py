
class MergeSort:

    def __init__(self):
        pass

    def merge_sort(self, array):
        if len(array) > 1:
            half = len(array) // 2
            left = array[:half]
            right = array[half:]

            self.merge_sort(left)
            self.merge_sort(right)

            i, j, k = 0, 0, 0

            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    array[k] = left[i]
                    i += 1
                else:
                    array[k] = right[j]
                    j += 1
                k += 1

            while i < len(left):
                array[k] = left[i]
                i += 1
                k += 1

            while j < len(right):
                array[k] = right[j]
                j, k = j + 1, k + 1

        return array

    @staticmethod
    def sort(array):
        return MergeSort().merge_sort(array)
