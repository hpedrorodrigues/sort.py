
class QuickSort:

    def __init__(self):
        pass

    @staticmethod
    def partition(array, left, right):
        i = left
        for j in range(left + 1, right + 1):
            if array[j] < array[left]:
                i += 1
                array[i], array[j] = array[j], array[i]
        array[i], array[left] = array[left], array[i]
        return i

    def quick_sort(self, array, left, right):
        if right > left:
            pivot_index = self.partition(array, left, right)
            self.quick_sort(array, left, pivot_index - 1)
            self.quick_sort(array, pivot_index + 1, right)
        return array

    @staticmethod
    def sort(array):
        if len(array) > 20000:
            return array
        return QuickSort().quick_sort(array, 0, len(array) - 1)
