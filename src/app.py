#!/usr/bin/env python

from array_generator import ArrayGenerator
from sort_time import SortTime
from csv_generator import CSVGenerator
import sys

# TAMANHOS_VTS_1 = range(1000, 10001, 1000)
# TAMANHOS_VTS_2 = range(10000, 100001, 10000)
# TAMANHOS_VTS_3 = range(100000, 1000001, 100000)

sizes = [
    1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,
    20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000,
    200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000
]
sys.setrecursionlimit(max(sys.getrecursionlimit(), 10000000))
csv_generator = CSVGenerator()
csv_generator.insert_header()

for size in sizes:

    array_generator = ArrayGenerator(size)

    asc_array = array_generator.asc()
    desc_array = array_generator.desc()
    random_array = array_generator.random()

    ascending_time = SortTime.times(array=asc_array)
    descending_time = SortTime.times(array=desc_array)
    random_time = SortTime.times(array=random_array)

    csv_generator.insert_time(size, ascending_time, descending_time, random_time)

    print "------------------------------------------------------------------"
    print "Size - Bubble Sort - Bubble Sort With Flag - Insertion Sort - " \
          "Selection Sort - Shell Sort - Merge Sort - Quick Sort - Counting Sort - Radix Sort"
    print "\nAscending time"
    print ascending_time
    print "\nDescending time"
    print descending_time
    print "\nRandom time"
    print random_time

csv_generator.close()
