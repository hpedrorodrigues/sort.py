
class CountingSort:

    def __init__(self):
        pass

    @staticmethod
    def counting_sort(array, max_value):
            m = max_value + 1
            cont = [0] * m
            for a in array:
                cont[a] += 1
            i = 0
            for a in range(m):
                for c in range(cont[a]):
                    array[i] = a
                    i += 1
            return array

    @staticmethod
    def sort(array):
        return CountingSort.counting_sort(array, max(array))
