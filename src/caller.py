
from bubble_sort import BubbleSort
from bubble_sort_with_flag import BubbleSortWithFlag
from insertion_sort import InsertionSort
from selection_sort import SelectionSort
from shell_sort import ShellSort
from merge_sort import MergeSort
from quick_sort import QuickSort
from counting_sort import CountingSort
from radix_sort import RadixSort
import copy
import timeit

class Caller:

    def __init__(self):
        pass

    @staticmethod
    def bubble_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        BubbleSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def bubble_sort_with_flag(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        BubbleSortWithFlag.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def insertion_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        InsertionSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def selection_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        SelectionSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def shell_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        ShellSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def merge_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        MergeSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def quick_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        QuickSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def counting_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        CountingSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')

    @staticmethod
    def radix_sort(array):
        start = timeit.default_timer()
        copied_array = copy.copy(array)
        RadixSort.sort(array=copied_array)
        end = timeit.default_timer()
        return format(end - start, '.5f')
