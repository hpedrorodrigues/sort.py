
class ArrayGenerator:

    def __init__(self, size):
        self.size = size
        self.array = range(1, size + 1)

    def asc(self):
        return self.array

    def desc(self):
        return self.array[::-1]

    def random(self):
        from random import randint

        return [randint(0, self.size) for _ in enumerate(self.array)]
